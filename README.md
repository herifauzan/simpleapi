# SimpleAPI

## Database Structure

employee
                 Table "public.employee"
 Column  |     Type      | Collation | Nullable | Default 
---------+---------------+-----------+----------+---------
 id      | bigint        |           | not null | 
 name    | text          |           | not null | 
 address | character(30) |           | not null | 
 npwp    | bigint        |           | not null | 

 history
                                  Table "public.history"
   Column    |     Type      | Collation | Nullable |               Default               
-------------+---------------+-----------+----------+-------------------------------------
 id          | integer       |           | not null | nextval('history_id_seq'::regclass)
 employee_id | bigint        |           | not null | 
 company_id  | bigint        |           | not null | 
 position    | character(30) |           | not null | 
 first_date  | date          |           | not null | 
 last_date   | date          |           |          | 

company
                  Table "public.company"
 Column  |     Type      | Collation | Nullable | Default 
---------+---------------+-----------+----------+---------
 id      | bigint        |           | not null | 
 name    | text          |           | not null | 
 address | character(50) |           |          | 
 npwp    | bigint        |           | not null | 



## DATABASE QUERY INSERT

\c api;
INSERT INTO employees (id, name, address, npwp) 
VALUES (1234, 'adi', 'jl mawar', 4321), 
(1235, 'budi', 'jl pinang', 5321),
(3231, 'supri', 'jl pohon', 1323);

INSERT INTO company VALUES (1111, 'ABC', 'jl mawar', 1111), 
(1222, 'BCD', 'jl pinang', 1222), (1333, 'XYZ', 'jl pohon', 1333);

INSERT INTO history (employee_id, company_id, position, first_date, last_date)
VALUES (1234, 1111, 'Backend Developer', '2017-10-2', '2019-2-18'),
(1234, 1333, 'Backend Developer', '2019-3-1', null),
(1235, 1111, 'Frontend Developer', '2016-7-7', '2018-7-7'),
(3231, 1111, 'Frontend Developer', '2018-7-10', null);

##USER TABLE
CREATE TABLE public.user(
    id int NOT NULL,
    username character(10) NOT NULL,
    passsword character(10),
    token text
);

INSERT INTO user (username, password) VALUES ('admin', 'sate');