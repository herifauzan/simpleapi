using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimpleApi.Models;


namespace SimpleApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private SimpleContext _context; 
        public CompanyController(SimpleContext context)
        {
            _context = context;

            if (_context.company.Count() == 0)
            {
                _context.company.Add(new Company { id = "5678", name="budi", address="jl cinta", npwp="8765" });
                _context.SaveChanges();
            }
        }
        // GET api/company , get all company
        [HttpGet]
        public ActionResult<IEnumerable<Company>> Get()
        {
            return _context.company.ToList();
        }

        // GET api/company/id
        [HttpGet("{id}")]
        public ActionResult<Company> Get(string id)
        {
            var item = _context.company.Find(id); 
            if (item == null) 
            { 
                return NotFound(); 
            } 
            return item;
        }
        // GET api/company/id
        [HttpGet("name/{name}")]
        public ActionResult<Company> GetByName(string name)
        {
            var item = _context.company.Where(x => x.name.Equals(name, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault(); 
            if (item == null) 
            { 
                return NotFound(); 
            } 
            return item;
        }

        // GET api/company/name yang masih kerja
        [HttpGet("current-employees/{id}")]
        public ActionResult<List<Employee>> GetCurrentEmployeesById(string id)
        {
            var history = _context.history.Where(x => x.company_id == id && (x.last_date == null)); 
            List<Employee> item= new List<Employee>();
            foreach(var x in history){
                String s= x.employee_id;
                var result=_context.employees.Find(s);
                if(!item.Contains(result)){
                    item.Add(result);
                }
            }
            if (item == null) 
            { 
                return NotFound(); 
            }
            return item;
        }
        
         // GET api/company/name yang masih kerja
        [HttpGet("employees/{id}")]
        public ActionResult<List<Employee>> GetEmployeesById(string id)
        {
            var history = _context.history.Where(x => x.company_id == id); 
            List<Employee> item= new List<Employee>();
            foreach(var x in history){
                String s= x.employee_id;
                var result=_context.employees.Find(s);
                if(!item.Contains(result)){
                    item.Add(result);
                }
            }
            if (item == null) 
            { 
                return NotFound(); 
            } 
            return item;
        }
        

        // POST api/company/
        [HttpPost]
        public void Post([FromBody] Company value)
        {
            if (_context.company.Where(x=> x.id == value.id).FirstOrDefault() == null)
            {
                _context.company.Add(new Company { id = value.id, name= value.name, address= value.address, npwp= value.npwp });
                _context.SaveChanges();
            }
        }

        // PUT api/values/5
        [HttpPatch("{id}")]
        public void Patch(int id, [FromBody] Company value)
        {
            var item= _context.company.Find(id);
            if(item != null){
                if(value.id != null){
                    item.id = value.id;
                }
                if(value.name != null ){
                    item.name = value.name;
                }
                if( value.address != null){
                    item.address = value.address;
                }
                if(value.npwp != null){
                    item.npwp = value.npwp;
                }
            }
            _context.Update(item);
            _context.SaveChanges();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            var check = _context.company.Where(x=> x.id.Equals(id)).FirstOrDefault();
            if(check != null) {
                _context.Remove(_context.company.Where(x=> x.id.Equals(id)).FirstOrDefault());
                _context.Remove(check);
                _context.SaveChanges();
            }
        }
    }
}
