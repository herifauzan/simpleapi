using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimpleApi.Models;

namespace SimpleApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private SimpleContext _context;
        public EmployeeController(SimpleContext context)
        {
            _context = context;

            if (_context.employees.Count() == 0)
            {
                _context.employees.Add(new Employee { id = "5678", name="budi", address="jl cinta", npwp="8765" });
                _context.SaveChanges();
            }
        }
        // GET api/employee, get all employee
        [HttpGet]
        public ActionResult<IEnumerable<Employee>> Get()
        {
            return _context.employees.ToList();
        }

        // GET api/employee/id, get employee by id
        [HttpGet("{id}")]
        public ActionResult<Employee> Get(string id)
        {
            var item = _context.employees.Find(id); 
            if (item == null) 
            { 
                return NotFound(); 
            } 
            return item;
        }

        // GET api/employee/id, get employee by id
        [HttpGet("name/{name}")]
        public ActionResult<Employee> GetByName(string name)
        {
            var item = _context.employees.Where(x => x.name.Equals(name, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault(); 
            if (item == null) 
            { 
                return NotFound(); 
            } 
            return item;
        }

         // GET api/employee/id, get employee by id
        [HttpGet("history/{id}")]
        public ActionResult<List<History>> GetHistory(string id)
        {
            var item = _context.history.Where(x => x.employee_id.Equals(id)).ToList(); 
            if (item == null) 
            { 
                return NotFound(); 
            } 
            return item;
        }

        //fungsi bantuan
        public List<Employee> getFriends(string id){
            var history = _context.history.Where(x => x.id.Equals(id) && x.last_date == null).FirstOrDefault();
            string company_id="";
            if(history != null){
                company_id = history.company_id;
            }
            var sameHistory = _context.history.Where(x => x.company_id.Equals(company_id) && x.last_date == null).ToList(); 
            List<Employee> item = new List<Employee>();
            foreach(var i in sameHistory){
                string iid= i.employee_id;
                Employee x= _context.employees.Where(j=> j.id.Equals(iid)).FirstOrDefault();
                if(!item.Contains(x)){
                    item.Add(x);
                }
            }
            return item;
        }

        [HttpGet("friends/{id}")]
        public ActionResult<List<Employee>> GetFriends(string id)
        {
            List<Employee> item = new List<Employee>();
            item= getFriends(id);
            if (item == null) 
            { 
                return NotFound(); 
            } 
            return getFriends(id);
        }

        [HttpGet("former/{id}")]
        public ActionResult<List<Employee>> GetFormer(string id)
        {
            var history = _context.history.Where(x => x.id.Equals(id));
            List<Employee> item= new List<Employee>();
            foreach(var h in history){
                var hs= _context.history.Where(l => l.company_id.Equals(h.company_id) && ((l.start_date < h.last_date) || (l.last_date > h.start_date)));
                foreach(var ee in hs){
                    Employee xe = _context.employees.Where(z=>z.id.Equals(ee.employee_id)).FirstOrDefault();
                    if(xe!= null && !item.Contains(xe)){
                        item.Add(xe);
                    }
                }
            }

            if (item == null) 
            { 
                return NotFound(); 
            } 
            return item;
        }

        [HttpGet("fof/{id}")]
        public ActionResult<List<Employee>> GetFriendsOfFriends(string id)
        {
            List<Employee> friends = getFriends(id);
            List<Employee> item = new List<Employee>();
            foreach(Employee e in friends){
                List<Employee> temp = getFriends(e.id);
                foreach(Employee fof in temp){
                    if(!item.Contains(fof) && !fof.id.Equals(id)){
                        item.Add(fof);
                    }
                }
            }
            Employee ini= _context.employees.Find(id);
            item.Remove(ini);//remove diri sendiri
            if (item == null) 
            { 
                return NotFound(); 
            } 
            return item;
        }

        // POST api/employee
        [HttpPost]
        public void Post([FromBody] Employee value)
        {
            if (_context.employees.Where(x=> x.id == value.id).FirstOrDefault() == null)
            {
                _context.employees.Add(new Employee { id = value.id, name= value.name, address= value.address, npwp= value.npwp });
                _context.SaveChanges();
            }
        }

        // PUT api/values/5
        [HttpPatch("{id}")]
        public void Put(int id, [FromBody] Employee value)
        {
            var item= _context.employees.Find(id);
            if(item != null){
                if(value.id != null){
                    item.id = value.id;
                }
                if(value.name != null ){
                    item.name = value.name;
                }
                if( value.address != null){
                    item.address = value.address;
                }
                if(value.npwp != null){
                    item.npwp = value.npwp;
                }
            }
            _context.Update(item);
            _context.SaveChanges();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var check = _context.employees.Where(x=> x.id.Equals(id)).FirstOrDefault();
            if(check != null) {
                
                _context.Remove(_context.employees.Where(x=> x.id.Equals(id)).FirstOrDefault());
                _context.Remove(check);
                _context.SaveChanges();
            }
            return Ok();
        }
    }
}
