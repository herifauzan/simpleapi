using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimpleApi.Models;

namespace SimpleApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HistoryController : ControllerBase
    {
        private SimpleContext _context; 
        public HistoryController(SimpleContext context)
        {
            _context = context;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<History>> Get()
        {
            return _context.history.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<History> Get(int id)
        {
            var history = _context.history.Where(x => x.id == id).FirstOrDefault();
            if(history == null){
                return NotFound();
            }else{
                return history;
            }
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] History value)
        {
            var history = _context.history.Where(x=> x.employee_id.Equals(value.employee_id) && x.company_id.Equals(value.company_id) && x.position.Equals(value.position)).FirstOrDefault();
            if ( history == null)
            {
                _context.history.Add(new History { employee_id= value.employee_id, company_id= value.company_id, position= value.position, start_date = value.start_date, last_date = value.last_date});
                _context.SaveChanges();
            }
        }

        // PUT api/values/5
        [HttpPatch("{id}")]
        public void Update(int id, [FromBody] History value)
        {
            var item= _context.history.Find(id);
            if(item != null){
                if(value.employee_id != null){
                    item.employee_id = value.employee_id;
                }
                if(value.company_id != null ){
                    item.company_id = value.company_id;
                }
                if( value.position != null){
                    item.position = value.position;
                }
                if(value.start_date != null){
                    item.start_date = value.start_date;
                }
                if(value.last_date != null){
                    item.last_date = value.last_date;
                }
            }
            _context.Update(item);
            _context.SaveChanges();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var check = _context.company.Where(x=> x.id.Equals(id)).FirstOrDefault();
            if(check != null) {
                _context.Remove(_context.history.Where(x=> x.id.Equals(id)).FirstOrDefault());
                _context.Remove(check);
                _context.SaveChanges();
            }
        }
    }
}
