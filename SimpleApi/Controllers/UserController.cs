using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SimpleApi.Models;

namespace SimpleApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "X-My-Header")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private SimpleContext _context; 
        public UserController(SimpleContext context)
        {
            _context = context;
            if (_context.user.Count() == 0)
            {
                _context.user.Add(new User { username="admin", password="admin" });
                _context.SaveChanges();
            }
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public ActionResult<string> Authenticate([FromBody]User userParam)
        {
            var user = _context.user.Where(x=> x.username.Equals(userParam.username) && x.password.Equals(userParam.password)).FirstOrDefault();
            if ( user == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }else
            {
                user.token = "abcd";
                _context.SaveChanges();
                return user.token;
            }
        }
    }
}