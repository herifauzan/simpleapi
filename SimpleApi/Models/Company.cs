namespace SimpleApi.Models
{
    public class Company
    {
        public string id {get; set;}
        public string name {get; set;}
        public string address {get; set;}
        public string npwp {get; set;}
    }
}