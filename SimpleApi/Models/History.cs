using System;

namespace SimpleApi.Models
{
    public class History
    {
        public int id {get; set;}
        public string employee_id { get; set;}
        public string position { get; set;}
        public string company_id { get; set;}
        public DateTime start_date { get; set;}
        public Nullable<DateTime> last_date { get; set;}
    }
}