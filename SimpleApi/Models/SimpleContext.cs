using Microsoft.EntityFrameworkCore;  
namespace SimpleApi.Models {     
    public class SimpleContext : DbContext     
    {         
        public SimpleContext(DbContextOptions<SimpleContext> options)                            : base(options)         
        { }       
        public DbSet<Employee> employees { get; set; }     
        public DbSet<History> history { get; set; }     
        public DbSet<Company> company { get; set; }     
        public DbSet<User> user { get; set; }
    } 
}